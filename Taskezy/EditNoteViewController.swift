//
//  EditNoteViewController.swift
//  Taskezy
//
//  Created by Way2MacMinM2 on 13/02/23.
//

import UIKit
import Toast

class EditNoteViewController: UIViewController {
    
    
    // MARK: defined outlets for ui
    @IBOutlet weak var noteTitle: UITextField!
    @IBOutlet weak var noteSubtitle: UITextField!
    @IBOutlet weak var noteDate: UILabel!
    @IBOutlet weak var noteMessage: UITextView!
    
    // MARK: variable instantiation
    var notePresenter:NotePresenter?;
    var note:NoteModel?
    var isediting:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(isediting != nil){
            if isediting!{
                self.title = "Edit Note"
                self.navigationItem.rightBarButtonItems = [UIBarButtonItem(image:UIImage(systemName: "trash"),style: .plain,target: self,action: #selector(deleteNote)),UIBarButtonItem(image:UIImage(systemName: "pencil.tip"),style: .plain,target: self,action: #selector(saveNote)) ]
            }else{
                self.title = "Add Note"
                self.navigationItem.rightBarButtonItems = [UIBarButtonItem(image:UIImage(systemName: "pencil.tip"),style: .plain,target: self,action: #selector(saveNote)) ]
            }
        }
        
        
        // Set up the back button
        let backButton = UIBarButtonItem()
//        backButton.title = "Back" // You can customize the back button title here
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
//        self.navigationItem.backButtonTitle = ""
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "dfa", style: .done, target: nil, action: nil)
        
        //        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:UIImage(systemName: "square.and.arrow.up.fill"),style: .plain,target: self,action: #selector(rightHandAction))
        //        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:UIImage(systemName: "trash"),style: .plain,target: self,action: #selector(rightHandAction))
        
        
        setData()
    }
    
    // MARK: Actions for Buttons
    @objc func deleteNote(_ sender:Any){
        if note != nil{
            deleteConfirmation(title: noteTitle.text!)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func saveNote(_ sender:Any){
        if !noteTitle.text!.isEmpty && !noteMessage.text.isEmpty{
            note = NoteModel(noteId: 1, title: noteTitle.text!, subTitle: noteSubtitle.text!, message: noteMessage.text!.trimmingCharacters(in: .whitespacesAndNewlines), date: Date())
            
            var res:Bool = false
            if isediting!{
                res = notePresenter!.onUpdateNote(model: note!)
            }else{
                res = notePresenter!.onAddNote(model: note!)
            }
            if res{
                showToast(msg: "Note Saved")
                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false, block: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
            }else{
//                print("failed to add note to list")
                showToast(msg: "failed to save note")
            }
        }else{
            showToast(msg: "title and message are empty")
//            print("title and message are empty")
        }
    }
    
    //MARK: helper functions
    func setData(){
        if(self.note != nil){
            noteTitle.text = note?.title
            noteSubtitle.text = note?.subTitle
            noteMessage.text = note?.message
            noteDate.text = setDateFormat(d: note?.date)
        }else{
            noteDate.text = setDateFormat(d: Date())
        }
    }
    
    func setDateFormat(d:Date?)->String{
        let format = DateFormatter()
        format.dateFormat = "dd MMM yyyy"
        return format.string(from: d!)
    }
    
    func deleteConfirmation(title:String){
        let alert = UIAlertController(title: title, message: "Are you sure to delete this note?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive,handler: {_ in
            let res = self.notePresenter!.onDeleteNote(noteId: self.note!.noteId)
            if res {
                self.showToast(msg: "Note Deleted")
                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false, block: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel,handler: {
            _ in
//            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true,completion:nil)
    }
    
    func showToast(msg:String){
        self.view.makeToast(msg)
    }
}
