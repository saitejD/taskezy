//
//  DBService.swift
//  Taskezy
//
//  Created by Way2MacMinM2 on 10/02/23.
//

import Foundation
import SQLite3

class DBService{
    let dbPath = "todoDb.sqflite"
    let tableName = "todo"
    var db:OpaquePointer?
    
    init() {
        self.db = openDatabase()
    }
    
    private func openDatabase()->OpaquePointer?{
        let filePath = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create:false).appendingPathComponent(dbPath)
        var db:OpaquePointer? = nil
        if sqlite3_open(filePath.path, &db) != SQLITE_OK{
            debugPrint("can't open database")
            return nil
        }else{
            print("successfully created connection to dabase")
            return db
        }
    }
    
    func createTable(){
        let createTableString = "CREATE TABLE IF NOT EXISTS \(tableName)(id INTEGER PRIMARY KEY,title varchar,subTitle varchar, message varchar,date integer);"
        var createTableStatement:OpaquePointer? = nil
        if sqlite3_prepare_v2(self.db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK{
            if sqlite3_step(createTableStatement) == SQLITE_DONE{
                print("\(tableName) table created")
            }else{
                print("\(tableName) table could not be created")
            }
        }else{
            print("\(tableName) statement could not be prepared")
        }
        sqlite3_finalize(createTableStatement)
    }
    
    
    func insert(model:NoteModel) -> Bool{
//        let d = Int(Date().timeIntervalSince1970)
//        print(Date(timeIntervalSince1970: TimeInterval(d)))
        let notes:[NoteModel] = read()
        var query:String = ""
        if notes.isEmpty{
            query = "INSERT INTO \(tableName)(id,title,subTitle,message,date) VALUES(1,'\(model.title)', '\(model.subTitle)', '\(model.message)',\(Int(model.date.timeIntervalSince1970)) );"
        }else{
            query = "INSERT INTO \(tableName)(title,subTitle,message,date) VALUES('\(model.title)', '\(model.subTitle)', '\(model.message)',\(Int(model.date.timeIntervalSince1970)) );"
        }
        
        var statement : OpaquePointer? = nil;
    
        if sqlite3_prepare_v2(self.db, query, -1, &statement,  nil) == SQLITE_OK{
//            if isEmpty{
//                sqlite3_bind_int(statement, 1, 1)
//            }
            //            sqlite3_bind_text(statement, 2, (model.title as NSString).utf8String, -1, nil)
            //            sqlite3_bind_text(statement, 3, (model.subTitle as NSString).utf8String, -1, nil)
            //            sqlite3_bind_text(statement, 4, (model.message as NSString).utf8String, -1, nil)
            
            if sqlite3_step(statement) == SQLITE_DONE{
//                print("data inserted")
                return true
            }else{
//                print("data not inserted")
            }
        }else{
            print("query is not in required format \(String(describing: query))")
        }
        return false
    }
    
    func read() -> [NoteModel]{
        var notesList:[NoteModel] = [NoteModel]()
        let query = "Select * from \(tableName) Order By date DESC;"
        var statement : OpaquePointer? = nil;
        
        if sqlite3_prepare_v2(self.db, query, -1, &statement,  nil) == SQLITE_OK{
            
            while sqlite3_step(statement) == SQLITE_ROW{
                let id = Int(sqlite3_column_int(statement,0))
                let title = String(describing: String(cString: sqlite3_column_text(statement, 1)))
                let subTitle = String(describing: String(cString: sqlite3_column_text(statement, 2)))
                let message = String(describing: String(cString: sqlite3_column_text(statement, 3)))
                let date = Date(timeIntervalSince1970: TimeInterval(sqlite3_column_int64(statement, 4)))
                notesList.append(NoteModel(noteId: id, title: title, subTitle: subTitle, message: message, date: date))
//                let format = DateFormatter()
//                format.dateFormat = "dd-MM-yyyy"
//                print(format.string(from: date))
                
            }
        }else{
            print("query is not in required format \(String(describing: query))")
        }
        return notesList
    }
    
    func update(model:NoteModel) ->Bool{
        let d = Int(model.date.timeIntervalSince1970)
        let query = "Update \(tableName) SET title = '\(model.title)', subTitle = '\(model.subTitle)', message = '\(model.message)', date = \(d) where id = \(model.noteId) ;"
        var statement : OpaquePointer? = nil;
        
        if sqlite3_prepare_v2(self.db, query, -1, &statement,  nil) == SQLITE_OK{
            if sqlite3_step(statement) == SQLITE_DONE{
//                print("data updated")
                return true
            }else{
                print("data not updated")
            }
        }else{
            print("query is not in required format \(String(describing: statement))")
        }
        return false
    }
    
    func delete(id : Int) -> Bool {
        let query = "DELETE FROM \(tableName) where id = \(id)"
        var statement : OpaquePointer? = nil
        if sqlite3_prepare_v2(db, query, -1, &statement, nil) == SQLITE_OK{
            if sqlite3_step(statement) == SQLITE_DONE {
                return true
            }else {
//                print("Data is not deleted in table")
            }
        }else{
            print("query is not in required format \(String(describing: query))")
        }
        return false
    }
    
    func search(term:String)->[NoteModel]{
        var notesList:[NoteModel] = [NoteModel]()
        let query = "Select * from \(tableName) where title like '%\(term)%' Order By date DESC;"
        var statement : OpaquePointer? = nil;
        
        if sqlite3_prepare_v2(self.db, query, -1, &statement,  nil) == SQLITE_OK{
            
            while sqlite3_step(statement) == SQLITE_ROW{
                let id = Int(sqlite3_column_int(statement,0))
                let title = String(describing: String(cString: sqlite3_column_text(statement, 1)))
                let subTitle = String(describing: String(cString: sqlite3_column_text(statement, 2)))
                let message = String(describing: String(cString: sqlite3_column_text(statement, 3)))
                let date = Date(timeIntervalSince1970: TimeInterval(sqlite3_column_int64(statement, 4)))
                notesList.append(NoteModel(noteId: id, title: title, subTitle: subTitle, message: message, date: date))
//                let format = DateFormatter()
//                format.dateFormat = "dd-MM-yyyy"
            }
            return notesList
            //            print(notesList.last?.noteId)
        }else{
            print("query is not in required format \(String(describing: query))")
        }
        return notesList
    }
    
    func dropTable(){
        let query = "DROP TABLE \(tableName)"
        var statement : OpaquePointer? = nil;
        if sqlite3_prepare_v2(self.db, query, -1, &statement,  nil) == SQLITE_OK{
            if sqlite3_step(statement) == SQLITE_DONE{
                print("table dropped")
            }else{
                print("table not dropped")
            }
        }else{
            print("query is not in required format \(String(describing: statement))")
        }
    }
}


