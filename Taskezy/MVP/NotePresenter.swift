//
//  NotePresenter.swift
//  Taskezy
//
//  Created by Way2MacMinM2 on 13/02/23.
//

import Foundation

class NotePresenter{
    weak var delegate:NoteDelegate?
    let dbService:DBService = DBService()
    
    init() {
        dbService.createTable()
    }
    
    func onLoaded(){
        let notes:[NoteModel] = dbService.read()
        delegate?.reloadTable(notesList: notes)
    }
    
    func onAddNote(model:NoteModel) -> Bool{
        let res:Bool = dbService.insert(model: model)
        if res{
            let notes:[NoteModel] = dbService.read()
            delegate?.reloadTable(notesList: notes)
            return res
        }else{
            return res
        }
    }
    
    func onUpdateNote(model:NoteModel) -> Bool{
        let res:Bool = dbService.update(model: model)
        if res{
            let notes:[NoteModel] = dbService.read()
            delegate?.reloadTable(notesList: notes)
            return res
        }else{
            return res
        }
    }
    func onDeleteNote(noteId:Int)->Bool{
        let res:Bool = dbService.delete(id: noteId)
        if res{
            let notes:[NoteModel] = dbService.read()
            delegate?.reloadTable(notesList: notes)
            return res
        }else{
            return res
        }
    }
    
    func onSearchNote(term:String) -> Bool{
        let notesList:[NoteModel] = dbService.search(term: term)
        delegate?.reloadTable(notesList: notesList)
        if !notesList.isEmpty{
            return true
        }
        return false
    }
}

protocol NoteDelegate:AnyObject{
    func reloadTable(notesList: [NoteModel])
}
