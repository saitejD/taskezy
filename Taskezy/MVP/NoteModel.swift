//
//  NoteModel.swift
//  Taskezy
//
//  Created by Way2MacMinM2 on 10/02/23.
//

import Foundation

struct NoteModel:Codable{
    let noteId:Int
    let title:String
    let subTitle:String
    let message:String
    let date:Date
    
    init(noteId: Int, title: String, subTitle: String, message: String, date: Date) {
        self.noteId = noteId
        self.title = title
        self.subTitle = subTitle
        self.message = message
        self.date = date
    }
}
