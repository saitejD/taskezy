//
//  NoteTableViewCell.swift
//  Taskezy
//
//  Created by Way2MacMinM2 on 13/02/23.
//

import UIKit

class NoteTableViewCell: UITableViewCell {

    @IBOutlet weak var noteTitle: UILabel!
    @IBOutlet weak var noteDate: UILabel!
    @IBOutlet weak var noteSubTitle: UILabel!
    @IBOutlet weak var noteMessage: UILabel!
    @IBOutlet weak var bgView:UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
