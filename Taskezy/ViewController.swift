//
//  ViewController.swift
//  Taskezy
//
//  Created by Way2MacMinM2 on 10/02/23.
//

import UIKit
class ViewController: UIViewController, NoteDelegate,UITextFieldDelegate {

    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var notesList: [NoteModel] = [NoteModel]()
    var isLoading = false
    var isSearching = false
    var progressHud:UIActivityIndicatorView?
    
    var noNotesView:UILabel?
    //    let dbService = DBService()
    //    var delegate: NoteDelegate?
    var notePresenter:NotePresenter?
    override func viewDidLoad() {
        super.viewDidLoad()
        noNotesView = UILabel(frame: CGRect(origin: CGPoint(x: 0, y: self.view.frame.height/2), size: CGSize(width: self.view.frame.size.width, height: 100)))
        noNotesView!.text = ""
        noNotesView!.textColor = .red
        noNotesView!.textAlignment = .center
        noNotesView!.font = noNotesView!.font.withSize(20)
        searchTF.delegate = self
        
        // Register the table view cell class and its reuse id
        self.tableView.register(UINib(nibName: "NoteTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        notePresenter = NotePresenter()
        notePresenter?.delegate = self
        isLoading = true
        progressHud = UIActivityIndicatorView(frame: CGRect(x: self.view.frame.width/2.5, y: view.frame.height/2, width: 100, height: 100))
        if(isLoading){
            progressHud!.startAnimating()
            self.view.addSubview(progressHud!)
        }
        notePresenter!.onLoaded()
        
        // navigationbar manipulation
//        self.navigationController!.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @IBAction func addNoteButtonClicked(_ sender: Any) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
//        navigationItem.backBarButtonItem = backItem
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditNoteViewController") as! EditNoteViewController
        vc.title = "Add Note"
        vc.isediting = false
        vc.notePresenter = self.notePresenter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onSearch(_ sender: Any) {
        isSearching = true
        if searchTF.text!.isEmpty{
            isSearching = false
        }
        let res:Bool = notePresenter!.onSearchNote(term: searchTF.text!)
        isLoading = false
        progressHud?.removeFromSuperview()
        if !res{
            print("search failed")
        }
    }

    func reloadTable(notesList: [NoteModel]) {
        self.notesList = notesList
        print("reload Table View")
        isLoading = false
        progressHud!.removeFromSuperview()
        tableView.reloadData()
        self.showNoNotesText()
    }
    
    func showNoNotesText(){
        if isSearching{
            noNotesView?.text = "No Notes found with given search term"
        }else{
            noNotesView?.text = "Notes list is empty. Write one to show"
        }
        if notesList.isEmpty{
            self.view.addSubview(noNotesView!)
        }else{
            noNotesView!.removeFromSuperview()
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension ViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:NoteTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NoteTableViewCell
        cell.noteTitle.text = notesList[indexPath.row].title
        cell.noteSubTitle.text = notesList[indexPath.row].subTitle
        cell.noteMessage.text = notesList[indexPath.row].message

        let format = DateFormatter()
        format.dateFormat = "dd MMM yyyy"
        cell.noteDate.text = format.string(from: notesList[indexPath.row].date)
        
        cell.bgView.backgroundColor = UIColor.init(white: 0.9, alpha: 0.8)
        cell.bgView.layer.cornerRadius = 10.0
        cell.bgView.layer.shadowColor = UIColor.gray.cgColor
        cell.bgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cell.bgView.layer.shadowRadius = 2.0
        cell.bgView.layer.shadowOpacity = 0.7
        
        cell.selectionStyle = .none

        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        notesList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
//        navigationItem.backBarButtonItem = backItem
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditNoteViewController") as! EditNoteViewController
        vc.title = "Add Note"
        vc.isediting = true
        vc.note = notesList[indexPath.row]
        vc.notePresenter = self.notePresenter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = UIContextualAction(style: .normal, title: "edit", handler: {
            _, _, _ in
            let backItem = UIBarButtonItem()
            backItem.title = ""
//            self.navigationItem.backBarButtonItem = backItem
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditNoteViewController") as! EditNoteViewController
            vc.title = "Add Note"
            vc.isediting = true
            vc.note = self.notesList[indexPath.row]
            vc.notePresenter = self.notePresenter
            self.navigationController?.pushViewController(vc, animated: true)
        })
        let delete = UIContextualAction(style: .destructive, title: "delete", handler: {
            _, _, _ in
            let _ = self.notePresenter!.onDeleteNote(noteId: self.notesList[indexPath.row].noteId)
        })
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [edit,delete])
        return swipeConfiguration
    }

}
